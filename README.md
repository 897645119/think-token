# think-token

#### 介绍
think-token 是为 thinkphp 6.0.* 提供的`token`机制方便大家零活运用

#### ThinkPHP

` >= thinkphp 6.0.*`


#### 安装教程

Composer
>composer require fanxd/think-token dev-master

#### 使用说明
`user_token`表
```
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jr_user_token
-- ----------------------------
DROP TABLE IF EXISTS `jr_user_token`;
CREATE TABLE `jr_user_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户id',
  `token` varchar(64) NOT NULL DEFAULT '' COMMENT 'token',
  `device_type` varchar(10) NOT NULL DEFAULT '' COMMENT '设备类型;mobile,android,iphone,ipad,web,pc,mac,wxapp',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `expire_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT ' 过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户客户端登录 token 表';
```
