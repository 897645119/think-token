<?php
/**
 * User: 假如_丶 <897645119@qq.com>
 * Date: 2020/5/12
 * Time: 15:08
 * 无可奈何花落去，似曾相识燕归来。
 */


namespace fanxd\token;


use fanxd\token\command\api\Create as TokenCreate;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands([
            TokenCreate::class,
        ]);
    }
}