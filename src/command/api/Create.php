<?php
/**
 * User: 假如_丶 <897645119@qq.com>
 * Date: 2020/5/12
 * Time: 17:26
 * 无可奈何花落去，似曾相识燕归来。
 */


namespace fanxd\token\command\api;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument as InputArgument;
use think\console\Output;

class Create extends Command
{
    protected function configure()
    {
        $this->setName('fanxd:token')
            ->setDescription('Create a new token')
            ->addArgument('name', InputArgument::REQUIRED, 'What is the name of the token?')
            ->setHelp(sprintf('%sCreates a new token%s', PHP_EOL, PHP_EOL));
    }

    protected function execute(Input $input, Output $output)
    {
        $path = $this->getPath();

        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        $path = realpath($path);

        $className = $input->getArgument('name');

        // Compute the file path
        $filePath = $path . DIRECTORY_SEPARATOR . $className . '.php';

        $contents = file_get_contents($this->getTemplate());

        $classes  = [
            'AuthClass' => $className,
        ];

        $contents = strtr($contents, $classes);

        if (false === file_put_contents($filePath, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $path));
        }

        $output->writeln('<info>created</info> .' . str_replace(getcwd(), '', $filePath));
    }

    protected function getTemplate()
    {
        $stubPath = __DIR__  . '/../stubs' . DIRECTORY_SEPARATOR;
        return $stubPath . 'middleware.stub';
    }

    protected function getPath()
    {
        return $this->app->getRootPath() . 'app\middleware' . DIRECTORY_SEPARATOR;
    }
}