<?php
/**
 * User: 假如_丶 <897645119@qq.com>
 * Date: 2020/5/13
 * Time: 11:14
 * 无可奈何花落去，似曾相识燕归来。
 */

use app\model\UserToken;
use Carbon\Carbon;


if (!function_exists('generateUserToken')) {
    function generateUserToken($userId, $deviceType)
    {
        $findUserToken  = UserToken::where('user_id', $userId)->where('device_type', $deviceType)->find();

        $currentTime    = Carbon::now();

        $expireTime     = $currentTime->addHour(24);

        $token          = md5(uniqid()) . md5(uniqid());
        if (empty($findUserToken)) {
            UserToken::create([
                'token'       => $token,
                'user_id'     => $userId,
                'create_time' => Carbon::now()->toDateTimeString(),
                'expire_time' => $expireTime->toDateTimeString(),
                'device_type' => $deviceType
            ]);
        } else {
            $expireTime = Carbon::parse($findUserToken['expire_time']);
            if ($expireTime->gte($currentTime) && !empty($findUserToken['token'])) {
                $token = $findUserToken['token'];
            } else {
                UserToken::where('user_id', $userId)
                    ->where('device_type', $deviceType)
                    ->update([
                        'token'       => $token,
                        'create_time' => Carbon::now()->toDateTimeString(),
                        'expire_time' => $expireTime->toDateTimeString()
                    ]);
            }

        }

        return $token;
    }
}